# Twitter URL Decoder

Userscript to extract real URLs instead of https://t.co/ URLS when using Twitter.

# Introduction

If you are the kind of person that right-clicks on links and chooses **Copy link address**
and then pastes it into a new tab to avoid sites tracking what you click on (via Javascript),
then this is the tool you need for Twitter.

In Google, DuckDuckGo, Bing and other sites with links, it is possible to use the above
workflow to avoid sending information about what you click on to the hosting site.
For Twitter, the links are of the form https://t.co/XXXX which is redirected by Twitter
servers to the actual link, so this is not possible.

If you would rather not have Twitter know what you are clicking on,
you can dig into the HTML page source and find the _**"title"**_ attribute,
but this is tedious; hence this user script.

# Installation

Install the user script manager:
- Firefox: [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey)
- Chrome: [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)

Click on the link to [Twitter URL Decoder.user.js](https://gitlab.com/derrickoswald/twitter-url-decoder/-/raw/master/Twitter%20URL%20Decoder.user.js).

Follow the instructions to add the user script, e.g.
- an installation dialog will appear
- review the sites on which this script will work (twitter.com)
- view the source of the script if so desired
- confirm that you wish to install the script

# Operation

When the user script is installed, and you access Twitter,
you should see a small form in the upper left hand corner:

![Twitter URL Decoder form](./form.png "User Interface Form")

Since Twitter is a single web page application, not all links are available immediately.
So, in order to find the URL's that are on the current page, click the **Refresh** button.
It will report how many links were found.
  
To access a link, hover over the https://t.co/ link you want, and the decoded URL will
be displayed in the form text box, and also as a simple link above the form.
Either click on the link above the form to open it in a new tab,
or click the **Copy** button to copy the link text to your clipboard
(or you can play with the link text in the text box).

As you scroll through Twitter, new content is added which may have new links,
so click the **Refresh** button again to access these new links if you
do not get the decoded link.
