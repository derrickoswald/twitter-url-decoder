// ==UserScript==
// @name         Twitter URL Decoder
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Display t.co URL as original.
// @author       Derrick Oswald
// @match        https://twitter.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // remove superfluous ellipse at end of URL
    function noEllipses (text)
    {
        return (text.endsWith ("…") ? text.substring (0, text.length - 1) : text);
    }

    // put URL into the UI
    function updateLinks (text)
    {
        const url = noEllipses (text);
        document.getElementById ("decoded_url_panel").innerHTML = url;
        document.getElementById ("decoded_url_link").innerHTML = "<a href='" + url + "' target='_blank'>" + url + "</a>";
    }

    // read link title text into the panel
    function translate (event)
    {
        const link = event.currentTarget;
        if (link)
        {
            const title = link.getAttribute ("title");
            if (title)
                updateLinks (title);
            else
            {
                const text = link.innerText;
                if (text && ("" !== text))
                    updateLinks (text);
            }
        }
    }

    // copy panel text to clipboard
    function copy (event)
    {
        const link = document.getElementById ("decoded_url_panel").innerHTML;
        navigator.clipboard.writeText (link);
    }

    // add listener on mouse over to links with href starting with https://t.co
    function refresh ()
    {
        const list = document.getElementsByTagName ("a");
        var count = 0;
        for (var i = 0; i < list.length; i++)
        {
            const link = list.item (i);
            if (link.href && link.href.startsWith ("https://t.co/"))
            {
                link.addEventListener ("mouseover", translate);
                count++;
            }
        }
        document.getElementById ("decoded_url_panel").innerHTML = "" + count + " links found";
    }

(
    function initialize ()
    {
        console.log ("initializing twitter URL decoder");
        // set up window
        const template =
`
    <div style='height: 100%'>
        <div style='width: 135px; padding-left: 20px; color: #337ab7;'>URL Decoder</div>
        <div id='decoded_url_link' style='max-width: 200px; min-width: 100px;'></div>
        <pre id='decoded_url_panel' style='max-width: 200px; min-width: 100px; overflow-x: auto; max-height: 25%; overflow-y: auto; border: 5px solid #e3edf9; padding: 1em; margin-bottom: 1em'></pre>
        <button id='refresh_url_panel' type='button' style='float: right; text-shadow: initial;'>Refresh</button>
        <button id='copy_url_panel' type='button' style='float: right; text-shadow: initial;'>Copy</button>
    </div>
`;
        const div = document.createElement ("div");
        div.setAttribute ("style", "position: fixed; top: 80px; left: 20px;");
        div.innerHTML = template;
        document.getElementsByTagName ("body")[0].appendChild (div);
        document.getElementById ("refresh_url_panel").addEventListener ("click", refresh);
        document.getElementById ("copy_url_panel").addEventListener ("click", copy);

        console.log ("finished initializing twitter URL decoder");
    }
)();

})();

